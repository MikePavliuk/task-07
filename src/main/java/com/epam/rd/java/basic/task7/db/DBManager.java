package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

    private static DBManager instance;
    private static final String CONNECTION_URL = getConnectionUrlFromPropertyFile();

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    private static final class Query {
        public static final String GET_USER_BY_LOGIN = "SELECT id, login FROM users WHERE login=?";
        public static final String GET_ALL_USERS = "SELECT id, login FROM users";
        public static final String INSERT_USER = "INSERT INTO users (id, login) VALUES (DEFAULT, ?)";
        public static final String DELETE_USER = "DELETE FROM users WHERE id=? AND login =?";

        public static final String GET_TEAM_BY_NAME = "SELECT id, name FROM teams WHERE name=?";
        public static final String GET_ALL_TEAMS = "SELECT id, name FROM teams";
        public static final String DELETE_TEAM = "DELETE FROM teams WHERE id=? AND name =?";
        public static final String INSERT_TEAM = "INSERT INTO teams (id, name) VALUES (DEFAULT, ?)";
        public static final String UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";

        public static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
        public static final String GET_USER_TEAMS =
                "SELECT teams.id, teams.name FROM teams " +
                        "JOIN users_teams " +
                        "ON teams.id = users_teams.team_id " +
                        "WHERE users_teams.user_id = ?";
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL);
             PreparedStatement stmt = conn.prepareStatement(Query.GET_ALL_USERS);
             ResultSet rs = stmt.executeQuery()) {

            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                users.add(user);
            }

        } catch (SQLException e) {
            throw new DBException("Exception while finding all users", e);
        }

        return users;
    }

    public boolean insertUser(User user) throws DBException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            pstmt = conn.prepareStatement(Query.INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, user.getLogin());
            if (pstmt.executeUpdate() > 0) {
                ResultSet rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    user.setId(rs.getInt(1));
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw new DBException("Exception occurred while inserting user", e);
        } finally {
            closeResource(pstmt);
            closeResource(conn);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL);
             PreparedStatement pstmt = conn.prepareStatement(Query.DELETE_USER)) {
            for (User user : users) {
                pstmt.setInt(1, user.getId());
                pstmt.setString(2, user.getLogin());
                pstmt.executeUpdate();
            }
            return true;
        } catch (SQLException e) {
            throw new DBException("Exception occurred while deleting users", e);
        }
    }

    public User getUser(String login) throws DBException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            pstmt = conn.prepareStatement(Query.GET_USER_BY_LOGIN);
            pstmt.setString(1, login);
            rs = pstmt.executeQuery();
            User user = null;
            if (rs.next()) {
                user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
            }
            return user;

        } catch (SQLException e) {
            throw new DBException("Exception occurred while getting user by login", e);
        } finally {
            closeResource(rs);
            closeResource(pstmt);
            closeResource(conn);
        }
    }

    public Team getTeam(String name) throws DBException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            pstmt = conn.prepareStatement(Query.GET_TEAM_BY_NAME);
            pstmt.setString(1, name);
            rs = pstmt.executeQuery();
            Team team = null;
            if (rs.next()) {
                team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
            }
            return team;

        } catch (SQLException e) {
            throw new DBException("Exception occurred while getting team by name", e);
        } finally {
            closeResource(rs);
            closeResource(pstmt);
            closeResource(conn);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL);
             PreparedStatement stmt = conn.prepareStatement(Query.GET_ALL_TEAMS);
             ResultSet rs = stmt.executeQuery()) {

            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teams.add(team);
            }

        } catch (SQLException e) {
            throw new DBException("Exception while finding all teams", e);
        }

        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            pstmt = conn.prepareStatement(Query.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, team.getName());
            if (pstmt.executeUpdate() > 0) {
                rs = pstmt.getGeneratedKeys();
                if (rs.next()) {
                    team.setId(rs.getInt(1));
                    return true;
                }
            }
            return false;
        } catch (SQLException e) {
            throw new DBException("Exception occurred while inserting team", e);
        } finally {
            closeResource(rs);
            closeResource(pstmt);
            closeResource(conn);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            conn.setAutoCommit(false);
            pstmt = conn.prepareStatement(Query.SET_TEAMS_FOR_USER);
            for (Team team : teams) {
                pstmt.setInt(1, user.getId());
                pstmt.setInt(2, team.getId());
                pstmt.executeUpdate();
            }
            conn.commit();
            return true;
        } catch (SQLException e) {
            if (conn != null) {
                try {
                    conn.rollback();
                    throw new DBException("Exception occurred while setting teams for user", e);
                } catch (SQLException ex) {
                    throw new DBException("Exception while rolling back transaction", ex);
                }
            }
        } finally {
            closeResource(conn);
            closeResource(pstmt);
        }
        return false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            conn = DriverManager.getConnection(CONNECTION_URL);
            pstmt = conn.prepareStatement(Query.GET_USER_TEAMS);
            pstmt.setInt(1, user.getId());
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teams.add(team);
            }

        } catch (SQLException e) {
            throw new DBException("Exception while finding all teams", e);
        } finally {
            closeResource(rs);
            closeResource(pstmt);
            closeResource(conn);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL);
             PreparedStatement pstmt = conn.prepareStatement(Query.DELETE_TEAM)) {
            pstmt.setInt(1, team.getId());
            pstmt.setString(2, team.getName());
            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DBException("Exception occurred while deleting team", e);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection conn = DriverManager.getConnection(CONNECTION_URL);
             PreparedStatement pstmt = conn.prepareStatement(Query.UPDATE_TEAM)) {
            pstmt.setInt(2, team.getId());
            pstmt.setString(1, team.getName());
            return pstmt.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DBException("Exception occurred while updating team", e);
        }
    }

    private static String getConnectionUrlFromPropertyFile() {
        try (var input = new FileInputStream("app.properties")) {

            Properties prop = new Properties();
            prop.load(input);
            return prop.getProperty("connection.url");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private void closeResource(AutoCloseable autoCloseable) {
        if (autoCloseable != null) {
            try {
                autoCloseable.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

}
